# Loopback Microservice

## Description

`/loopback-microservice0` is the "model" part of the software architecture. Imagine that it's the part behind the API Gateway. The service runs on port `:3000`.
`/loopback-microservice1` is the "service" part of the software architecture. Imagine that it's the part in front of the API Gateway requesting some operations from a service behind the gateway. The service runs on port `:3001`.

## Background

This repository is meant to show how a microservice software architecture could be created using the Loopback framework.
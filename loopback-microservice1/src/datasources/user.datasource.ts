import {inject, lifeCycleObserver, LifeCycleObserver} from '@loopback/core';
import {juggler} from '@loopback/repository';

const config = {
  name: 'User',
  connector: 'rest',
  baseURL: '',
  crud: false,
  options: {
    headers: {
      'content-type': 'application/json'
    }
  },
  operations: [
    {
      template: {
        method: 'GET',
        url: 'http://localhost:3000/users',
        headers: {
          'content-type': 'application/json',
          'Authorization': '{token}'
        },
      },
      functions: {
        getAllUsers: ['token']
      }
    },
    {
      template: {
        method: 'GET',
        url: 'http://localhost:3000/users/{id}',
        headers: {
          'content-type': 'application/json',
          'Authorization': '{token}'
        },
      },
      functions: {
        getUser: ['token','id']
      }
    },
    {
      template: {
        method: 'DELETE',
        url: 'http://localhost:3000/users/{id}',
        headers: {
          'content-type': 'application/json',
          'Authorization': '{token}'
        },
      },
      functions: {
        deleteUser: ['token','id']
      }
    },
    {
      template: {
        method: 'POST',
        url: 'http://localhost:3000/users',
        headers: {
          'content-type': 'application/json',
          'Authorization': '{token}'
        },
        body: '{user:object}'
      },
      functions: {
        createUser: ['token','user']
      }
    },
    {
      template: {
        method: 'PUT',
        url: 'http://localhost:3000/users/{id}',
        headers: {
          'content-type': 'application/json',
          'Authorization': '{token}'
        },
        body: '{user:object}'
      },
      functions: {
        updateUser: ['token','id','user']
      }
    }
  ]
};

// Observe application's life cycle to disconnect the datasource when
// application is stopped. This allows the application to be shut down
// gracefully. The `stop()` method is inherited from `juggler.DataSource`.
// Learn more at https://loopback.io/doc/en/lb4/Life-cycle.html
@lifeCycleObserver('datasource')
export class UserDataSource extends juggler.DataSource
  implements LifeCycleObserver {
  static dataSourceName = 'User';
  static readonly defaultConfig = config;

  constructor(
    @inject('datasources.config.User', {optional: true})
    dsConfig: object = config,
  ) {
    super(dsConfig);
  }
}

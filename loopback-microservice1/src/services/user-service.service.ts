import {getService} from '@loopback/service-proxy';
import {inject, Provider} from '@loopback/core';
import {UserDataSource} from '../datasources';
import {User} from '../models';

export interface UserService {
  // this is where you define the Node.js methods that will be
  // mapped to REST/SOAP/gRPC operations as stated in the datasource
  // json file.

  getAllUsers(token: string): Promise<User[]>;
  getUser(token: string, id: number): Promise<User>;
  createUser(token: string, user: User): Promise<User>;
  updateUser(token: string, id: number, user: User): Promise<User>;
  deleteUser(token: string, id: number): Promise<User>;
}

export class UserServiceProvider implements Provider<UserService> {
  constructor(
    // User must match the name property in the datasource json file
    @inject('datasources.User')
    protected dataSource: UserDataSource = new UserDataSource(),
  ) {}

  value(): Promise<UserService> {
    return getService(this.dataSource);
  }
}

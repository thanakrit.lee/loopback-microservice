// Uncomment these imports to begin using these cool features!

import {inject} from '@loopback/context';
import {get, param, post, put, del, getModelSchemaRef, requestBody, operation} from '@loopback/rest';
import {UserService} from '../services';
import {User} from '../models';


export class UserController {
  constructor(
    @inject('services.UserService')
    protected userService: UserService,
  ) {}

  @get('/users', {
    parameters: [{name: 'Authorization', in: 'header', required: true}],
    responses: {
      '200': {
        description: 'Array of User model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(User, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async getAllUsers(
    @param.header.string('Authorization') token: string
  ): Promise <User[]> {
    let users: User[];
    users = await this.userService.getAllUsers(token);
    return users;
  }


  @get('/users/{id}',{
    parameters: [{name: 'Authorization', in: 'header', required: true}],
    responses: {
      '200': {
        description: 'User model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(User, {includeRelations: true}),
          },
        },
      },
    },
  })
  async getUser(
    @param.header.string('Authorization') token: string,
    @param.path.number('id') id: number
  ): Promise <User> {
    let user: User;
    user = await this.userService.getUser(token, id);
    return user;
  }

  @post('/users', {
    parameters: [{name: 'Authorization', in: 'header', required: true}],
    responses: {
      '200': {
        description: 'User model instance',
        content: {'application/json': {schema: getModelSchemaRef(User)}},
      },
    },
  })
  async createUser(
    @param.header.string('Authorization') token: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(User, {
            title: 'NewUser',
            exclude: ['id'],
          }),
        },
      },
    })
    user: Omit<User, 'id'>,
    ): Promise <User> {
    let respUser: User;
    respUser = await this.userService.createUser(token, user);
    return respUser;
  }

  @put('/users/{id}', {
    parameters: [{name: 'Authorization', in: 'header', required: true}],
    responses: {
      '204': {
        description: 'User PUT success',
      },
    },
  })
  async updateUser(
    @param.header.string('Authorization') token: string,
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(User, {
            title: 'NewUser',
            exclude: ['id'],
          }),
        },
      },
    })
    user: Omit<User, 'id'>,
  ): Promise <User> {
    let respUser: User;
    respUser = await this.userService.updateUser(token, id, user);
    return respUser;
  }

  @del('/users/{id}', {
    parameters: [{name: 'Authorization', in: 'header', required: true}],
    responses: {
      '204': {
        description: 'User DELETE success',
      },
    },
  })
  async deleteUser(
    @param.header.string('Authorization') token: string,
    @param.path.number('id') id: number,
  ): Promise <User> {
    let respUser: User;
    respUser = await this.userService.deleteUser(token, id);
    return respUser;
  }
}
